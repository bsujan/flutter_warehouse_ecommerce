import 'package:flutter/material.dart';
import 'package:warehouse/screens/view/rate_rinse_experience.dart';

class BottomSheetWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _BottomSheetWidgetState();
}

class _BottomSheetWidgetState extends State<BottomSheetWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 390,
      decoration: BoxDecoration(
        color: Color(0xFF1D6076),
      ),
      child: Column(
        children: <Widget>[
          RateRinseExperience(),
        ],
      ),
    );
  }
}
