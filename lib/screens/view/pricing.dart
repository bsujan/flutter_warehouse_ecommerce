import 'package:flutter/material.dart';
import 'package:warehouse/models/product.dart';
import 'package:warehouse/screens/view/home.dart';
// import 'package:xenome/firebase_services/authentication.dart';
// import 'package:xenome/firebase_services/follow_status.dart';
// import 'package:xenome/screens/profile/my_profile.dart';
// import 'package:xenome/screens/profile/others_profile.dart';
// import 'package:xenome/verification/login_check.dart';
// import 'package:xenome/utils/session_manager.dart';
// import 'package:xenome/screens/view/home.dart';

class Pricing extends StatefulWidget {
  // final BaseAuth auth;
  // final VoidCallback loginCallback;
  // final String uid;
  // final String wheretype;
  // FollowingStatus({this.auth, this.loginCallback, this.uid, this.wheretype});

  @override
  _PricingState createState() => _PricingState();
}

class _PricingState extends State<Pricing> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  List<Products> dryCleanList = [];
  List<Products> washFoldList = [];
  List<Products> hangDryList = [];
  int _currentIndex = 3;

  @override
  void initState() {
    super.initState();
    getDryCleanProduct();
    getWashFoldProduct();
    getHangDryProduct();
  }

  void onTabTapped(int index) async {
    setState(() {
      _currentIndex = index;
    });

    switch (_currentIndex) {
      case 0:
        {}
        break;

      case 1:
        {}
        break;

      case 2:
        {
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => Home()));
        }
        break;

      case 3:
        {
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => Pricing()));
        }
        break;

      case 4:
        {
          // Navigator.pushReplacement(
          //     context, MaterialPageRoute(builder: (context) => MyProfile()));
        }
        break;

      default:
        {
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => Home()));
        }
    }
  }

  getDryCleanProduct() async {
    for (int i = 0; i < 6; i++) {
      Products _tempDryCleanList = new Products(
        uid: '',
        imageURL: 'assets/images/pic0.jpg',
        productName: 'Sweater',
        price: double.parse(i.toString()),
      );
      dryCleanList.add(_tempDryCleanList);
    }
  }

  getWashFoldProduct() async {
    for (int i = 0; i < 6; i++) {
      Products _tempWashFoldList = new Products(
        uid: '',
        imageURL: 'assets/images/pic1.jpg',
        productName: '',
        price: double.parse(i.toString()),
      );
      washFoldList.add(_tempWashFoldList);
    }
  }

  getHangDryProduct() async {
    for (int i = 0; i < 6; i++) {
      Products _tempHangDryList = new Products(
        uid: '',
        imageURL: 'assets/images/pic2.jpg',
        productName: '',
        price: double.parse(i.toString()),
      );
      hangDryList.add(_tempHangDryList);
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            title: const Text('Pricing'),
            bottom: TabBar(
              tabs: <Widget>[
                Text(
                  'Dry Clean',
                  style: TextStyle(
                    fontSize: 16,
                    fontFamily: 'Roboto Medium',
                    color: Color(0xFFFFFFFF),
                  ),
                ),
                Text(
                  'Wash & Fold',
                  style: TextStyle(
                    fontSize: 16,
                    fontFamily: 'Roboto Medium',
                    color: Color(0xFFFFFFFF),
                  ),
                ),
                Text(
                  'Hang Dry',
                  style: TextStyle(
                    fontSize: 16,
                    fontFamily: 'Roboto Medium',
                    color: Color(0xFFFFFFFF),
                  ),
                ),
              ],
            ),
            backgroundColor: Color(0xFF315E73),
          ),
          body: TabBarView(
            children: <Widget>[
              Container(
                padding: const EdgeInsets.fromLTRB(0.0, 25.0, 0.0, 55.0),
                child: _buildDryCleanListChild(context),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(0.0, 25.0, 0.0, 55.0),
                child: _buildWashFoldListChild(context),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(0.0, 25.0, 0.0, 55.0),
                child: _buildHangDryListChild(context),
              ),
            ],
          ),
          backgroundColor: Color(0xFFF1F0EE),
          bottomNavigationBar: Theme(
            data: Theme.of(context).copyWith(
                canvasColor: Color(0xFFFFFFFF),
                primaryColor: Colors.white,
                textTheme: Theme.of(context)
                    .textTheme
                    .copyWith(caption: TextStyle(color: Color(0xFF868E9C)))),
            child: BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              selectedItemColor: Color(0xFFD68337),
              currentIndex: _currentIndex,
              onTap: onTabTapped,
              items: [
                BottomNavigationBarItem(
                  icon: Icon(Icons.local_offer, size: 26),
                  title: Text('Order'),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.schedule, size: 26),
                  title: Text('Schedule'),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.account_circle, size: 26),
                  title: Text('Account'),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.print, size: 26),
                  title: Text('Pricing'),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.help_outline, size: 26),
                  title: Text('Help'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildDryCleanListChild(BuildContext context) {
    return Container(
      color: Color(0xFFF1F0EE),
      child: new ListView(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                  padding: const EdgeInsets.fromLTRB(20.0, 0.0, 0.0, 5.0),
                  child: Text(
                    'Skillfully cleaned and pressed Returned on a hanger. We follow the care label to ensure the best results. \$30 order minimum',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w300,
                    ),
                  )),
              Container(
                  padding: EdgeInsets.only(top: 20, left: 20, bottom: 20),
                  child: Text(
                    'Most frequently ordered',
                    style: TextStyle(
                      color: Color(0xFF1D6076),
                      fontSize: 20,
                    ),
                  )),
              Container(
                padding: EdgeInsets.only(top: 20, left: 0, bottom: 0, right: 0),
                constraints: const BoxConstraints(maxHeight: 350.0),
                child: ListView.builder(
                  itemBuilder: _buildMostOrderedForDryClean,
                  itemCount: dryCleanList.length,
                  scrollDirection: Axis.vertical,
                ),
              ),

              // _buildMostFrequentlyOrderedChild(),
              // _buildCommentChild(),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildMostOrderedForDryClean(BuildContext context, int index) {
    return Container(
      height: 60,
      color: Colors.white,
      child: Row(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
            child: Image.asset(
              dryCleanList[index].imageURL,
              width: 50,
              height: 50,
            ),
          ),
          Expanded(
            child: Text(
              dryCleanList[index].productName,
              textAlign: TextAlign.left,
              style: TextStyle(
                fontSize: 18,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(right: 20, top: 10, bottom: 10),
            child: Text(
              '\$' + dryCleanList[index].price.toString(),
              style: TextStyle(
                fontSize: 18,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildWashFoldListChild(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 10.0, 0.0, 5.0),
    );
  }

  Widget _buildHangDryListChild(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 10.0, 0.0, 5.0),
    );
  }
}
