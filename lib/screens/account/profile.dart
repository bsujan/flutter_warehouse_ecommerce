import 'package:flutter/material.dart';
import 'package:warehouse/screens/view/home.dart';

class Profile extends StatefulWidget {
  Profile({Key key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

enum SingingCharacter { all, scheduleAndservice, service, none }

class _ProfileState extends State<Profile> {
  SingingCharacter _character;

  @override
  void initState() {
    super.initState();
    _character = SingingCharacter.all;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF1F0EE),
      appBar: PreferredSize(
        preferredSize: Size(null, 100),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(color: Color(0xFF315E73), spreadRadius: 5, blurRadius: 2)
          ]),
          width: MediaQuery.of(context).size.width,
          height: 100,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(1.0),
            child: Container(
              color: Colors.transparent,
              child: Container(
                margin: EdgeInsets.fromLTRB(10, 50, 10, 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (_) => Home()),
                          );
                        },
                        child: Icon(
                          Icons.arrow_back,
                          color: Colors.orange[300],
                          size: 24.0,
                        )),
                    Text(
                      ' My account',
                      style: TextStyle(
                        fontSize: 20,
                        fontFamily: 'Roboto Medium',
                        color: Colors.orange,
                      ),
                    ),
                    Text(
                      '    Profile',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 24,
                        fontFamily: 'Roboto Medium',
                        color: Color(0xFFFFFFFF),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
      body: new Container(
        child: ListView(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.fromLTRB(20, 10, 20, 20),
              child: Text('Text notification',
                  style: TextStyle(
                      fontFamily: 'Roboto Black',
                      fontWeight: FontWeight.w600,
                      fontSize: 18,
                      color: Color(0xFF1D6076))),
            ),
            _buildTextNotificationChild(context),
            Container(
              padding: const EdgeInsets.fromLTRB(20, 20, 20, 10),
              child: Text('Who are we serving?',
                  style: TextStyle(
                      fontFamily: 'Roboto Black',
                      fontWeight: FontWeight.w600,
                      fontSize: 18,
                      color: Color(0xFF1D6076))),
            ),
            _buildServingChild(context),
            Container(
              padding: const EdgeInsets.fromLTRB(20, 20, 20, 10),
              child: Text('Change Password',
                  style: TextStyle(
                      fontFamily: 'Roboto Black',
                      fontWeight: FontWeight.w600,
                      fontSize: 18,
                      color: Color(0xFF1D6076))),
            ),
            _buildPasswordChild(context),
          ],
        ),
      ),
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith(
            canvasColor: Color(0xFFFFFFFF),
            primaryColor: Colors.white,
            textTheme: Theme.of(context)
                .textTheme
                .copyWith(caption: TextStyle(color: Color(0xFF868E9C)))),
        child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          selectedItemColor: Color(0xFFD68337),
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.local_offer, size: 26),
              title: Text('Order'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.schedule, size: 26),
              title: Text('Schedule'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.account_circle, size: 26),
              title: Text('Account'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.print, size: 26),
              title: Text('Pricing'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.help_outline, size: 26),
              title: Text('Help'),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildTextNotificationChild(context) => Container(
        margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
        decoration: BoxDecoration(
          color: Colors.white, //remove color to make it transpatent
        ),
        child: Column(
          children: <Widget>[
            RadioListTile<SingingCharacter>(
              title: Padding(
                padding: const EdgeInsets.only(top: 0.0, bottom: 0.0),
                child: const Text(
                    'All scheduling, service-related, and special offer alerts'),
              ),
              value: SingingCharacter.all,
              groupValue: _character,
              onChanged: (SingingCharacter value) {
                setState(() {
                  _character = value;
                });
              },
            ),
            RadioListTile<SingingCharacter>(
              title: Padding(
                padding: const EdgeInsets.only(top: 0.0, bottom: 0.0),
                child: const Text('scheduling and service-related alerts only'),
              ),
              value: SingingCharacter.scheduleAndservice,
              groupValue: _character,
              onChanged: (SingingCharacter value) {
                setState(() {
                  _character = value;
                });
              },
            ),
            RadioListTile<SingingCharacter>(
              title: Padding(
                padding: const EdgeInsets.only(top: 0.0, bottom: 0.0),
                child: const Text('Service-related alerts only'),
              ),
              value: SingingCharacter.service,
              groupValue: _character,
              onChanged: (SingingCharacter value) {
                setState(() {
                  _character = value;
                });
              },
            ),
            RadioListTile<SingingCharacter>(
              title: Padding(
                padding: const EdgeInsets.only(top: 0.0, bottom: 0.0),
                child: const Text('None'),
              ),
              value: SingingCharacter.none,
              groupValue: _character,
              onChanged: (SingingCharacter value) {
                setState(() {
                  _character = value;
                });
              },
            ),
          ],
        ),
      );

  Widget _buildServingChild(context) => Container(
        margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
        padding: EdgeInsets.only(top: 20, bottom: 20),
        decoration: BoxDecoration(
          color: Colors.white, //remove color to make it transpatent
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(
              width: 30,
            ),
            Expanded(
              flex: 2,
              child: MaterialButton(
                onPressed: () {},
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    IconTheme(
                      data: new IconThemeData(
                        color: Color(0xFF868E9C),
                      ),
                      child: new Icon(Icons.perm_identity, size: 26),
                    ),
                    Text(
                      'Just me',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 15,
                        fontFamily: 'Roboto Medium',
                        color: Color(0xFF868E9C),
                      ),
                    )
                  ],
                ),
                color: Colors.transparent,
                elevation: 0,
                minWidth: 120,
                height: 80,
                padding: EdgeInsets.all(0),
                textColor: Colors.grey,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                    side: BorderSide(color: Colors.black26)),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              flex: 3,
              child: MaterialButton(
                onPressed: () {},
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        IconTheme(
                          data: new IconThemeData(
                            color: Color(0xFF868E9C),
                          ),
                          child: new Icon(Icons.perm_identity, size: 26),
                        ),
                        IconTheme(
                          data: new IconThemeData(
                            color: Color(0xFF868E9C),
                          ),
                          child: new Icon(Icons.perm_identity, size: 26),
                        ),
                      ],
                    ),
                    Text(
                      'A couple',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 15,
                        fontFamily: 'Roboto Medium',
                        color: Color(0xFF868E9C),
                      ),
                    )
                  ],
                ),
                color: Colors.transparent,
                elevation: 0,
                minWidth: 120,
                height: 80,
                padding: EdgeInsets.all(0),
                textColor: Colors.grey,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                    side: BorderSide(color: Colors.black26)),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              flex: 3,
              child: MaterialButton(
                onPressed: () {},
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        IconTheme(
                          data: new IconThemeData(
                            color: Color(0xFF868E9C),
                          ),
                          child: new Icon(Icons.perm_identity, size: 26),
                        ),
                        IconTheme(
                          data: new IconThemeData(
                            color: Color(0xFF868E9C),
                          ),
                          child: new Icon(Icons.perm_identity, size: 26),
                        ),
                        IconTheme(
                          data: new IconThemeData(
                            color: Color(0xFF868E9C),
                          ),
                          child: new Icon(Icons.perm_identity, size: 26),
                        ),
                      ],
                    ),
                    Text(
                      'A family',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 15,
                        fontFamily: 'Roboto Medium',
                        color: Color(0xFF868E9C),
                      ),
                    )
                  ],
                ),
                color: Colors.transparent,
                elevation: 0,
                minWidth: 120,
                height: 80,
                padding: EdgeInsets.all(0),
                textColor: Colors.grey,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                    side: BorderSide(color: Colors.black26)),
              ),
            ),
            SizedBox(
              width: 30,
            ),
          ],
        ),
      );

  Widget _buildPasswordChild(context) => Container(
        child: Column(
          children: <Widget>[
            Card(
              margin: EdgeInsets.fromLTRB(0, 0, 0, 1),
              child: ListTile(
                leading: const Icon(
                  Icons.lock,
                  size: 24.0,
                ),
                contentPadding:
                    EdgeInsets.only(top: 0, bottom: 0, left: 15, right: 15),
                title: Text('Current Password',
                    style: TextStyle(
                      fontFamily: 'Roboto Black',
                      fontWeight: FontWeight.w400,
                      fontSize: 18,
                    )),
              ),
            ),
            Card(
              margin: EdgeInsets.fromLTRB(0, 0, 0, 1),
              child: ListTile(
                leading: const Icon(
                  Icons.lock,
                  size: 24.0,
                ),
                contentPadding:
                    EdgeInsets.only(top: 0, bottom: 0, left: 15, right: 15),
                title: Text('New Password',
                    style: TextStyle(
                      fontFamily: 'Roboto Black',
                      fontWeight: FontWeight.w400,
                      fontSize: 18,
                    )),
              ),
            ),
            Card(
              margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
              child: ListTile(
                leading: const Icon(
                  Icons.lock,
                  size: 24.0,
                ),
                contentPadding:
                    EdgeInsets.only(top: 0, bottom: 0, left: 15, right: 15),
                title: Text('Confirm New Password',
                    style: TextStyle(
                      fontFamily: 'Roboto Black',
                      fontWeight: FontWeight.w400,
                      fontSize: 18,
                    )),
              ),
            ),
            Card(
              color: Colors.orange[300],
              margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
              child: MaterialButton(
                onPressed: () {},
                child: Text(
                  'Save',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: 'Roboto Black',
                      fontWeight: FontWeight.w400,
                      fontSize: 18,
                      color: Colors.white),
                ),
                color: Colors.transparent,
                elevation: 0,
                minWidth: MediaQuery.of(context).size.width,
                height: 60,
                padding: EdgeInsets.all(0),
                
              ),
            )
          ],
        ),
      );
}
