import 'package:flutter/material.dart';
import 'package:warehouse/screens/view/home.dart';

class ShareRinse extends StatefulWidget {
  ShareRinse({Key key}) : super(key: key);

  @override
  _ShareRinseState createState() => _ShareRinseState();
}

class _ShareRinseState extends State<ShareRinse> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF1F0EE),
      appBar: PreferredSize(
        preferredSize: Size(null, 100),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(color: Color(0xFF315E73), spreadRadius: 5, blurRadius: 2)
          ]),
          width: MediaQuery.of(context).size.width,
          height: 100,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(1.0),
            child: Container(
              color: Colors.transparent,
              child: Container(
                margin: EdgeInsets.fromLTRB(10, 50, 10, 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (_) => Home()),
                          );
                        },
                        child: Icon(
                          Icons.arrow_back,
                          color: Colors.orange[300],
                          size: 24.0,
                        )),
                    Text(
                      ' My account',
                      style: TextStyle(
                        fontSize: 20,
                        fontFamily: 'Roboto Medium',
                        color: Colors.orange,
                      ),
                    ),
                    Text(
                      '  Share Rinse',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 24,
                        fontFamily: 'Roboto Medium',
                        color: Color(0xFFFFFFFF),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.fromLTRB(20, 30, 20, 20),
              child: Text("Free Rinse",
                  style: TextStyle(
                      fontFamily: 'Roboto Black',
                      fontSize: 18,
                      color: Color(0xFF1D6076))),
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 20),
              child: Text(
                "Give \$ 15 to a friend and get \$ 15 for yourself when they complete a Rinse",
                style: TextStyle(
                  fontFamily: 'Roboto Black',
                  fontSize: 20,
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(15, 15, 15, 15),
              color: Colors.white,
              child: Text(
                "www.rinse.com/?kid=46TWRRVBIS",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontFamily: 'Roboto Black',
                    fontSize: 20,
                    color: Colors.orange[400]),
              ),
            ),
            GestureDetector(
              onTap: () {},
              child: Container(
                margin: EdgeInsets.fromLTRB(0, 30, 0, 20),
                padding: const EdgeInsets.fromLTRB(15, 15, 15, 15),
                color: Colors.orange,
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.email, size: 24, color: Colors.white),
                      Padding(
                        padding: EdgeInsets.only(left: 10),
                        child: Text(
                          'Email',
                          style: TextStyle(fontSize: 20, color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {},
              child: Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
                padding: const EdgeInsets.fromLTRB(15, 15, 15, 15),
                color: Colors.orange,
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.sms, size: 24, color: Colors.white),
                      Padding(
                        padding: EdgeInsets.only(left: 10),
                        child: Text(
                          'SMS',
                          style: TextStyle(fontSize: 20, color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {},
              child: Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
                padding: const EdgeInsets.fromLTRB(15, 15, 15, 15),
                color: Colors.orange,
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.share, size: 24, color: Colors.white),
                      Padding(
                        padding: EdgeInsets.only(left: 10),
                        child: Text(
                          'Share',
                          style: TextStyle(fontSize: 20, color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {},
              child: Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
                padding: const EdgeInsets.fromLTRB(15, 15, 15, 15),
                color: Colors.orange,
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.sms, size: 24, color: Colors.white),
                      Padding(
                        padding: EdgeInsets.only(left: 10),
                        child: Text(
                          'Tweet',
                          style: TextStyle(fontSize: 20, color: Colors.white),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith(
            canvasColor: Color(0xFFFFFFFF),
            primaryColor: Colors.white,
            textTheme: Theme.of(context)
                .textTheme
                .copyWith(caption: TextStyle(color: Color(0xFF868E9C)))),
        child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          selectedItemColor: Color(0xFFD68337),
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.local_offer, size: 26),
              title: Text('Order'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.schedule, size: 26),
              title: Text('Schedule'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.account_circle, size: 26),
              title: Text('Account'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.print, size: 26),
              title: Text('Pricing'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.help_outline, size: 26),
              title: Text('Help'),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildDescriptionChild() => Container(
      padding: EdgeInsets.all(15),
      color: Colors.white,
      child: Text(
        "Get unlimited free pickups and deliveries for all your Rinse order. Rinse Go waives the \$7.95 standard service fee for the duration of your subscription",
        style: TextStyle(fontSize: 18),
      ));

  Widget _buildSideBySideButton(context) => Container(
      margin: EdgeInsets.fromLTRB(0, 0, 0, 40),
      padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
      decoration: BoxDecoration(
        color: Colors.white, //remove color to make it transpatent
      ),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              SizedBox(width: MediaQuery.of(context).size.width * 0.1),
              Expanded(
                flex: 2,
                child: SizedBox(
                  height: MediaQuery.of(context).size.height * 0.12,
                  child: RaisedButton(
                    color: Color(0xFF315E73),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Monthly',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                          ),
                        ),
                        Text(
                          "\$7.95/year",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                          ),
                        ),
                      ],
                    ),
                    onPressed: () {},
                  ),
                ),
              ),
              SizedBox(width: MediaQuery.of(context).size.width * 0.03),
              Expanded(
                flex: 2,
                child: SizedBox(
                    height: MediaQuery.of(context).size.height * 0.12,
                    child: RaisedButton(
                      color: Color(0xFF315E73),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "Annual ",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                            ),
                          ),
                          Text(
                            "\$7.95/year",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                            ),
                          ),
                          Text(
                            "\$6.58/mo",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                      onPressed: () {},
                    )),
              ),
              SizedBox(width: MediaQuery.of(context).size.width * 0.1),
            ],
          ),
        ],
      ));
}
