import 'package:flutter/material.dart';
import 'package:warehouse/screens/view/home.dart';

class RinseRepeat extends StatefulWidget {
  RinseRepeat({Key key}) : super(key: key);

  @override
  _RinseRepeatState createState() => _RinseRepeatState();
}

class _RinseRepeatState extends State<RinseRepeat> {
  bool isMonthlyPlan = true;
  bool isAnnualPlan = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF1F0EE),
      appBar: PreferredSize(
        preferredSize: Size(null, 100),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(color: Color(0xFF315E73), spreadRadius: 5, blurRadius: 2)
          ]),
          width: MediaQuery.of(context).size.width,
          height: 100,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(1.0),
            child: Container(
              color: Colors.transparent,
              child: Container(
                margin: EdgeInsets.fromLTRB(10, 50, 10, 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (_) => Home()),
                          );
                        },
                        child: Icon(
                          Icons.arrow_back,
                          color: Colors.orange[300],
                          size: 24.0,
                        )),
                    Text(
                      ' My account',
                      style: TextStyle(
                        fontSize: 20,
                        fontFamily: 'Roboto Medium',
                        color: Colors.orange,
                      ),
                    ),
                    Text(
                      '    Rinse Repeat',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 24,
                        fontFamily: 'Roboto Medium',
                        color: Color(0xFFFFFFFF),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
      body: new Container(
        child: ListView(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
              child: Text("What's Rinse Repeat",
                  style: TextStyle(
                      fontFamily: 'Roboto Black',
                      fontWeight: FontWeight.w600,
                      fontSize: 18,
                      color: Color(0xFF1D6076))),
            ),
            _buildDescriptionChild(),
            Container(
              padding: const EdgeInsets.fromLTRB(20, 10, 20, 20),
              child: Text("Pricing",
                  style: TextStyle(
                      fontFamily: 'Roboto Black',
                      fontWeight: FontWeight.w600,
                      fontSize: 18,
                      color: Color(0xFF1D6076))),
            ),
            _buildSideBySideButton(context),
          ],
        ),
      ),
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith(
            canvasColor: Color(0xFFFFFFFF),
            primaryColor: Colors.white,
            textTheme: Theme.of(context)
                .textTheme
                .copyWith(caption: TextStyle(color: Color(0xFF868E9C)))),
        child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          selectedItemColor: Color(0xFFD68337),
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.local_offer, size: 26),
              title: Text('Order'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.schedule, size: 26),
              title: Text('Schedule'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.account_circle, size: 26),
              title: Text('Account'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.print, size: 26),
              title: Text('Pricing'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.help_outline, size: 26),
              title: Text('Help'),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildDescriptionChild() => Container(
      padding: EdgeInsets.all(15),
      color: Colors.white,
      child: Text(
        "Our Wash & Fold subscription. Priced by the bag, not by the pound. If it fits, it'll get washed. FREE     Next day Rush Delivery included!,",
        style: TextStyle(fontSize: 16),
      ));

  Widget _buildSideBySideButton(context) => Container(
      margin: EdgeInsets.fromLTRB(0, 0, 0, 40),
      padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
      decoration: BoxDecoration(
        color: Colors.white, //remove color to make it transpatent
      ),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              SizedBox(width: MediaQuery.of(context).size.width * 0.1),
              Expanded(
                flex: 2,
                child: SizedBox(
                  height: MediaQuery.of(context).size.height * 0.07,
                  child: isMonthlyPlan == true
                      ? RaisedButton(
                          color: Color(0xFF00A3AD),
                          child: Text(
                            "Monthly plans",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.w500),
                          ),
                          onPressed: () {
                            setState(() {
                              isMonthlyPlan = false;
                              isAnnualPlan = true;
                            });
                          },
                        )
                      : RaisedButton(
                          shape: RoundedRectangleBorder(
                              side: BorderSide(color: Color(0xFF00A3AD))),
                          color: Color(0xFFFFFFFF),
                          child: Text(
                            "Monthly plans",
                            style: TextStyle(
                                color: Color(0xFF00A3AD),
                                fontSize: 18,
                                fontWeight: FontWeight.w500),
                          ),
                          onPressed: () {
                            setState(() {
                              isMonthlyPlan = true;
                              isAnnualPlan = false;
                            });
                          },
                        ),
                ),
              ),
              Expanded(
                flex: 2,
                child: SizedBox(
                  height: MediaQuery.of(context).size.height * 0.07,
                  child: isAnnualPlan == false
                      ? RaisedButton(
                          shape: RoundedRectangleBorder(
                              side: BorderSide(color: Color(0xFF00A3AD))),
                          color: Colors.white,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "Annual plans",
                                style: TextStyle(
                                    color: Color(0xFF00A3AD),
                                    fontSize: 18,
                                    fontWeight: FontWeight.w500),
                              ),
                              Text(
                                "2 months free!",
                                style: TextStyle(
                                    color: Color(0xFF00A3AD),
                                    fontSize: 14,
                                    fontWeight: FontWeight.w300),
                              ),
                            ],
                          ),
                          onPressed: () {
                            setState(() {
                              isAnnualPlan = true;
                              isMonthlyPlan = false;
                            });
                          },
                        )
                      : RaisedButton(
                          color: Color(0xFF00A3AD),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Text(
                                "Annual plans",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w500),
                              ),
                              Text(
                                "2 months free!",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w300),
                              ),
                            ],
                          ),
                          onPressed: () {
                            setState(() {
                              isAnnualPlan = false;
                              isMonthlyPlan = true;
                            });
                          },
                        ),
                ),
              ),
              SizedBox(width: MediaQuery.of(context).size.width * 0.1),
            ],
          ),
          Container(
            margin: EdgeInsets.only(top: 40, bottom: 0),
            padding: EdgeInsets.all(0),
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 20),
                  child: Text(
                    '1 bag',
                    style: TextStyle(fontSize: 20),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text('\$60/mo', style: TextStyle(fontSize: 20)),
                      Text(
                        '\$60/bag',
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.w200),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 20, bottom: 0),
            padding: EdgeInsets.all(0),
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 20),
                  child: Text(
                    '2 bag',
                    style: TextStyle(fontSize: 20),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text('\$112/mo', style: TextStyle(fontSize: 20)),
                      Text(
                        '\$56/bag',
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.w200),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 20, bottom: 0),
            padding: EdgeInsets.all(0),
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 20),
                  child: Text(
                    '3 bag',
                    style: TextStyle(fontSize: 20),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text('\$159/mo', style: TextStyle(fontSize: 20)),
                      Text(
                        '\$53/bag',
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.w200),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 20, bottom: 20),
            padding: EdgeInsets.all(0),
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 20),
                  child: Text(
                    '4 bag',
                    style: TextStyle(fontSize: 20),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text('\$200/mo', style: TextStyle(fontSize: 20)),
                      Text(
                        '\$50/bag',
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.w200),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Card(
              color: Colors.orange[600],
              margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
              child: MaterialButton(
                onPressed: () {},
                child: Text(
                  'Subscribe to Rinse Repeat ',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: 'Roboto Black',
                      fontWeight: FontWeight.w600,
                      fontSize: 24,
                      color: Colors.white),
                ),
                color: Colors.transparent,
                elevation: 0,
                minWidth: MediaQuery.of(context).size.width,
                height: 60,
                padding: EdgeInsets.all(0),
              ),
            ),
          )
        ],
      ));
}
