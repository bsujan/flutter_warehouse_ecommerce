import 'package:flutter/material.dart';

import 'package:scoped_model/scoped_model.dart';
import 'package:warehouse/scoped_models/app_model.dart';
import 'package:warehouse/splash.dart';
import 'package:warehouse/theme.dart';

// @override
// Widget build(BuildContext context) {
//   // TODO: implement build
//   return null;
// }

class WareHouseApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    

    //SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    return ScopedModelDescendant<AppModel>(
        builder: (context, child, model) => MaterialApp(
              title: 'Warehouse',
              theme: buildTheme(),
              home: SplashScreen(),
            ));
  }
}
